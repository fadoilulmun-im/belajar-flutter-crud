import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:management_asset/data/repository/barangs.dart';
import 'package:management_asset/data/source/files.dart';
import 'package:management_asset/model/barang_model.dart';
import 'package:management_asset/model/barangs_model.dart';

class BarangImplementation extends BarangRepository {
  BarangImplementation(this._files);

  final Files _files;
  late final String path = 'barang.json';

  @override
  Future<void> deleteAllBarang() async {
    _files.remove(path);
  }

  @override
  Future<void> deleteBarang(String id) async {
    final barangs = await loadBarangs();
    final newBarangs =
        barangs.values.where((element) => element.id != id).toList();

    await _files.write(path, jsonEncode(Barangs(values: newBarangs).toJson()));
  }

  @override
  Future<Barang?> getByIdBarang(String id) async {
    final barangs = await loadBarangs();

    return barangs.values.firstWhereOrNull((element) => element.id == id);
  }

  @override
  Future<Barangs> loadBarangs() async {
    final contents = await _files.read(path);
    if(contents.isEmpty) return const Barangs(values: []);

    return Barangs.fromJson(jsonDecode(contents));
  }

  @override
  Future<void> saveBarang(Barang barang) async {
    final barangs = await loadBarangs();
    final newBarang = barangs.values.where((element) => element.id != barang.id).toList();

    newBarang.add(barang);

    await _files.write(path, jsonEncode(Barangs(values: newBarang).toJson()));
  }
}
