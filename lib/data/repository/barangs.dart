import 'package:management_asset/model/barang_model.dart';
import 'package:management_asset/model/barangs_model.dart';

abstract class BarangRepository {
  Future<Barangs> loadBarangs();
  Future<void> saveBarang(Barang barang);
  Future<void> deleteBarang(String id);
  Future<Barang?> getByIdBarang(String id);
  Future<void> deleteAllBarang();
}