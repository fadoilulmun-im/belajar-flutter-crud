import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:management_asset/data/repository/barangs.dart';
import 'package:management_asset/data/repository/barangs_implementation.dart';
import 'package:management_asset/data/source/module.dart';

final barangProvider = Provider<BarangRepository>((ref) {
  return BarangImplementation(ref.read(filesProvider));
});