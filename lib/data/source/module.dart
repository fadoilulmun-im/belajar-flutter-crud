import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:management_asset/data/source/files.dart';
import 'package:management_asset/data/source/files_implementation.dart';

final filesProvider = Provider<Files>((ref) {
  return FilesImplementation();
});