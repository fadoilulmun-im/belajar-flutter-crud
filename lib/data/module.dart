import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:management_asset/data/repository/module.dart';
import 'package:management_asset/model/barang_model.dart';
import 'package:management_asset/model/barangs_model.dart';

class BarangStateNotifier extends StateNotifier<Barangs> {
  final Ref ref;
  late final getBarangs = ref.read(barangProvider);

  BarangStateNotifier(this.ref) : super(const Barangs(values: []));

  Future<void> loadBarangs() async {
    state = await getBarangs.loadBarangs();
  }

  Future<void> save(Barang barang) async {
    await getBarangs.saveBarang(barang);
    await loadBarangs();
  }

  Future<Barang?> getById(String id) async {
    return await getBarangs.getByIdBarang(id);
  }

  Future<void> delete(String id) async {
    await getBarangs.deleteBarang(id);
    await loadBarangs();
  }
}

final barangListState = StateNotifierProvider.autoDispose<BarangStateNotifier, Barangs>((ref) {
  return BarangStateNotifier(ref);
});