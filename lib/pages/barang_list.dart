import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:management_asset/data/module.dart';

class BarangList extends ConsumerWidget {
  const BarangList({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final barang = ref.watch(barangListState);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Barang List'),
        actions: [
          IconButton(
            icon: const Icon(Icons.add),
            onPressed: () {
              context.go('/barang/new');
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            for (final b in barang.values)
              ListTile(
                title: Text(b.name),
                subtitle: Text(b.description),
                leading: Checkbox(
                  value: b.complete,
                  onChanged: (value) {
                    if(value != null) {
                      ref.read(barangListState.notifier).save(b.copyWith(complete: value));
                    }
                  },
                ),
                onTap: () => context.go('/barang/${b.id}'),
              )
          ],
        ),
      ),
    );
  }
}
