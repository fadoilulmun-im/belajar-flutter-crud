import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:management_asset/data/module.dart';
import 'package:management_asset/model/barang_model.dart';
import 'package:uuid/uuid.dart';

class BarangCreateEdit extends ConsumerStatefulWidget {
  final String? id;
  const BarangCreateEdit({super.key, this.id});

  @override
  ConsumerState<BarangCreateEdit> createState() => _BarangCreateEditState();
}

class _BarangCreateEditState extends ConsumerState<BarangCreateEdit> {
  final _formKey = GlobalKey<FormState>();
  final name = TextEditingController();
  final description = TextEditingController();
  bool isCompleted = false;

  var uuid = const Uuid();
  late final model = ref.read(barangListState.notifier);

  @override
  void initState() {
    super.initState();
    if (widget.id != null) {
      model.getById(widget.id!).then((barang) {
        if (mounted) {
          name.text = barang!.name;
          description.text = barang.description;
          setState(() {
            isCompleted = barang.complete!;
          });
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.id != null ? 'Edit Barang' : 'Tambah Barang'),
        actions: [
          if (widget.id != null)
            IconButton(
              icon: const Icon(Icons.delete),
              onPressed: () async {
                final router = GoRouter.of(context);
                final confirm = await showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: const Text('Apakah anda yakin?'),
                      content:
                        const Text('Data yang dihapus tidak dapat dikembalikan'),
                      actions: [
                        TextButton(
                          onPressed: () => Navigator.of(context).pop(false),
                          child: const Text('Cancel'),
                        ),
                        TextButton(
                          onPressed: () => Navigator.of(context).pop(true),
                          child: const Text('Delete'),
                        ),
                      ],
                    );
                  },
                );

                if (confirm == true) {
                  model.delete(widget.id!);
                  if(router.canPop()) router.pop();
                }
              },
            )
        ],
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                TextFormField(
                  controller: name,
                  decoration: const InputDecoration(labelText: 'Name'),
                  validator: (value) =>
                      value!.isEmpty ? 'Name is required' : null,
                ),
                TextFormField(
                  controller: description,
                  maxLines: 3,
                  decoration: const InputDecoration(labelText: 'Description'),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Description is required';
                    }
                    if (value.length < 3) {
                      return 'Description must be at least 3 characters';
                    }
                    return null;
                  },
                ),
                CheckboxListTile(
                  value: isCompleted,
                  onChanged: (value) {
                    if (mounted) {
                      setState(() {
                        isCompleted = value!;
                      });
                    }
                  },
                )
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          if (_formKey.currentState!.validate()) {
            final barang = Barang(
              id: widget.id ?? uuid.v4(),
              name: name.text,
              description: description.text,
              complete: isCompleted,
            );
            ref.read(barangListState.notifier).save(barang);
            context.pop();
          }
        },
        label: const Text('Save'),
      ),
    );
  }
}
