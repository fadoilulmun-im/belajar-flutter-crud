// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'barang_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$BarangImpl _$$BarangImplFromJson(Map<String, dynamic> json) => _$BarangImpl(
      id: json['id'] as String,
      name: json['name'] as String,
      description: json['description'] as String,
      complete: json['complete'] as bool? ?? false,
    );

Map<String, dynamic> _$$BarangImplToJson(_$BarangImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'complete': instance.complete,
    };
